import Vue from "vue";
import VueRouter from "vue-router";

import Home from '../components/Home';
import Desk from '../components/desk/Desk';
import ShowDesk from '../components/desk/ShowDesk';

Vue.use(VueRouter);

const routes = [
    {
        path: '/',
        name: 'home',
        component: Home
    },
    {
        path: '/desk',
        name: 'desk',
        component: Desk
    },
    {
        path: '/desk/:deskId',
        name: 'showDesk',
        component: ShowDesk,
        props: true
    },

];

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
});

export default router;

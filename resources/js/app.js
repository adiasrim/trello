import App from "./components/App";

require('./bootstrap');

import Vue from 'vue';
import router from "./helpers/router";

Vue.component('app', require('./components/App').default)
const app = new Vue({
    el: '#app',
    component: App,
    router
})

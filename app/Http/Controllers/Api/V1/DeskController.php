<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\DeskRequest;
use App\Http\Resources\DeskResource;
use App\Models\Desk;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class DeskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */

    public function index(): JsonResponse
    {
        $desk = Desk::with('lists')->latest()->get();
        return $this->responseResourceful(DeskResource::class, $desk);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param DeskRequest $request
     * @return JsonResponse
     */

    public function store(DeskRequest $request): JsonResponse
    {
        new DeskResource(Desk::create($request->validated()));
        return $this->responseMessage('New desk created successfully', Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param Desk $desk
     * @return JsonResponse
     */

    public function show(Desk $desk): JsonResponse
    {
        return $this->responseResourceful(DeskResource::class, $desk);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param DeskRequest $request
     * @param Desk $desk
     * @return JsonResponse
     */

    public function update(DeskRequest $request, Desk $desk): JsonResponse
    {
        $desk->update($request->validated());

        return $this->responseMessage($desk->name . ' successfully edited');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Desk $desk
     * @return JsonResponse
     */

    public function destroy(Desk $desk): JsonResponse
    {
        $desk->delete();

        return $this->responseMessage($desk->name . ' successfully deleted');
    }
}

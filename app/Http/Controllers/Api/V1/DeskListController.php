<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\DeskListRequest;
use App\Http\Resources\DeskListResource;
use App\Models\Desk;
use App\Models\DeskList;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class DeskListController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param DeskListRequest $request
     * @param Desk $desk
     * @return JsonResponse
     */
    public function store(DeskListRequest $request, Desk $desk): JsonResponse
    {
        $desk->lists()->create($request->validated());

        return $this->responseMessage('New list created successfully', Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param Desk $desk
     * @param DeskList $list
     * @return JsonResponse
     */
    public function show(Desk $desk, DeskList $list): JsonResponse
    {
        return $this->responseResourceful(DeskListResource::class, $list);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Desk $desk
     * @param DeskList $list
     * @param DeskListRequest $request
     * @return JsonResponse
     */
    public function update(Desk $desk, DeskList $list, DeskListRequest $request): JsonResponse
    {
        $list->update($request->validated());

        return $this->responseMessage($list->name . ' successfully edited');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DeskList $deskList
     * @return JsonResponse
     */
    public function destroy(DeskList $deskList): JsonResponse
    {
        $deskList->delete();

        return $this->responseMessage($deskList->name . ' successfully deleted');
    }
}

<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\CardRequest;
use App\Http\Resources\CardResource;
use App\Models\Card;
use App\Models\Desk;
use App\Models\DeskList;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class CardController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param Desk $desk
     * @param DeskList $list
     * @param CardRequest $request
     * @return JsonResponse
     */
    public function store(Desk $desk, DeskList $list, CardRequest $request): JsonResponse
    {
        $list->cards()->create($request->validated());

        return $this->responseMessage('New card created successfully', Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param Desk $desk
     * @param DeskList $list
     * @param Card $card
     * @return JsonResponse
     */
    public function show(Desk $desk, DeskList $list, Card $card): JsonResponse
    {
        return $this->responseResourceful(CardResource::class, $card);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Desk $desk
     * @param DeskList $list
     * @param Card $card
     * @param CardRequest $request
     * @return JsonResponse
     */
    public function update(Desk $desk, DeskList $list, Card $card, CardRequest $request): JsonResponse
    {
        $card->update($request->validated());

        return $this->responseMessage($card->name . ' successfully updated', Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Desk $desk
     * @param DeskList $list
     * @param Card $card
     * @return JsonResponse
     */
    public function destroy(Desk $desk, DeskList $list, Card $card): JsonResponse
    {
        $card->delete();

        return $this->responseMessage($card->name . ' has been deleted');
    }
}

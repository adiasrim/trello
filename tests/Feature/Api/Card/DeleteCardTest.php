<?php

namespace Tests\Feature\Api\Card;

use App\Models\Desk;
use Tests\TestCase;

class DeleteCardTest extends TestCase
{
    public function test_delete_a_card()
    {
        $desk = Desk::inRandomOrder()->first();
        $list = $desk->lists()->inRandomOrder()->first();
        $card = $list->cards()->inRandomOrder()->first();

        $this->assertDatabaseHas('cards', ['id' => $card->id]);

        $this->deleteJson(route('desks.lists.cards.destroy', [$desk->id, $list->id, $card->id]))->assertOk();

        $this->assertDatabaseMissing('cards', ['id' => $card->id]);
    }
}

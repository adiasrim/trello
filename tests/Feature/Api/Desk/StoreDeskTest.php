<?php

namespace Tests\Feature\Api\Desk;

use App\Models\Desk;
use Arr;
use Tests\TestCase;

class StoreDeskTest extends TestCase
{
    public function test_store_desk()
    {
        $data = Desk::factory()->make()->only(['name']);

        $this->postJson(route('desks.store'), $data)->assertCreated();

        $this->assertDatabaseHas('desks', Arr::only($data, 'name'));
    }
}

<?php

namespace Tests\Feature\Api\List;

use App\Models\Desk;
use Tests\TestCase;

class DeleteListTest extends TestCase
{
    public function test_delete_a_list()
    {
        $desk = Desk::inRandomOrder()->first();
        $list = $desk->lists()->inRandomOrder()->first();
        $this->assertDatabaseHas('desk_lists', ['id' => $list->id]);

        $this->deleteJson(route('desks.lists.destroy', [$desk->id, $list->id]))->assertOk();

        $this->assertDatabaseMissing('desk_lists', [['id' => $list->id], ['desk_id' => $desk->id]]);
    }
}

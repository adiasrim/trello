<?php

namespace Tests\Feature\Api\List;

use App\Models\Desk;
use App\Models\DeskList;
use Tests\TestCase;

class UpdateListTest extends TestCase
{
    public function test_update_a_list()
    {
        $desk = Desk::inRandomOrder()->first();
        $list = $desk->lists()->inRandomOrder()->first();
        $changes = [
            'name' => 'updated list'
        ];

        $this->patchJson(route('desks.lists.update', [$desk->id, $list->id]), $changes)->assertOk();

        $this->assertDatabaseHas('desk_lists', $changes);
        $this->assertTrue(DeskList::whereName($changes['name'])->exists());
    }
}

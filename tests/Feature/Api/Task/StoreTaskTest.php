<?php

namespace Tests\Feature\Api\Task;

use App\Models\Desk;
use Tests\TestCase;

class StoreTaskTest extends TestCase
{
    public function test_store_a_task()
    {
        $desk = Desk::inRandomOrder()->first();
        $list = $desk->lists()->inRandomOrder()->first();
        $card = $list->cards()->inRandomOrder()->first();

        $this->postJson(route('desks.lists.cards.tasks.store', [
            $desk->id,
            $list->id,
            $card->id
        ]), [
            'name' => 'new Task'
        ])->assertCreated();

        $this->assertDatabaseHas('tasks', [
            'name' => 'new Task'
        ]);
    }
}

<?php

namespace Tests\Feature\Api\Task;

use App\Models\Desk;
use Tests\TestCase;

class DeleteTaskTest extends TestCase
{
    public function test_delete_a_task()
    {
        $desk = Desk::inRandomOrder()->first();
        $list = $desk->lists()->inRandomOrder()->first();
        $card = $list->cards()->inRandomOrder()->first();
        $task = $card->tasks()->inRandomOrder()->first();

        $this->assertDatabaseHas('tasks', ['id' => $task->id]);
        $this->deleteJson(route('desks.lists.cards.tasks.destroy', [
            $desk->id,
            $list->id,
            $card->id,
            $task->id
        ]))->assertOk();

        $this->assertDatabaseMissing('tasks', ['id' => $task->id]);
    }
}
